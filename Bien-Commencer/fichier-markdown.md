# Partie 1

## Sous-partie 1 : texte

Une phrase sans rien

*Une phrase en italique*

**Une phrase en gras**

Un lien vers [fun-mocc.fr](https://www.fun-mooc.fr/)

Une ligne de `code`

## Sous-partie 2 : listes

**Liste à puces**
* item
    * sous-item
    * sous-item
* item
* item

**Liste numérotée**
1. item
1. item
1. item

## Sous-partir 3 : code

```
# extrait de code
```
